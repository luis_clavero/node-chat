var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "/app/app/controllers";
var incidentsController = require(path.join(controllerDir, "incidents"));

// Return the list of incidents
router.route("/").get(async (req, res, next) => {
    // Get all incidents
    const incidents = await incidentsController.getAllIncidents(req);
    res.render('incidentsList.pug', {incidents: incidents});
});

router.get("/new", function(req, res, next) {
    res.render('newIncident.pug');
});

router.post("/new", async (req, res, next) => {
    await incidentsController.addIncident(req);
    res.redirect("/incidents");
});

router.post("/delete/:id", async (req, res, next) => {
    await incidentsController.deleteIncidentById(req);
    res.redirect("/incidents"); 
});

router.get("/update/:id", async (req, res, next) => {
    incident = await incidentsController.getIncidentById(req);
    res.render('updateIncident.pug', {incident: incident});
});

router.post("/update/:id", async (req, res, next) => {
    incident = await incidentsController.updateIncidentById(req);
    res.redirect("/incidents");
});

// Export this file
module.exports = router;