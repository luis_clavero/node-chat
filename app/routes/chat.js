var express = require("express");
const session = require("express-session");
var router = express.Router();
var path = require("path");
var controllerDir = "/app/app/controllers";
var chatController = require(path.join(controllerDir, "chat"));
var chatController = require(path.join(controllerDir, "chat"));

// Return all chat messages
router.get("/", async (req, res, next) => {
    // If the username is not defined add redirect the user to the view to set it up
    if(req.session.username == undefined || req.session.username == "" ) {
        res.render('username.pug')
    } else {
        // Send user 
        var messages = await chatController.getChatMessagesByRoom("room1");
        console.log(messages);
        console.log(req.session.username);
        res.render('chat.pug', {user: req.session.username, chatMessages: messages});
    }
});

router.post("/", async (req, res, next) => {
    await chatController.addUsername(req);
    
    res.redirect("/chat");
});
// Export this file
module.exports = router;