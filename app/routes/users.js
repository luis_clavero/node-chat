var express = require("express");
var router = express.Router();

// Return the list of Users
router.get("/", async function(req, res, next) {
    // Wait for the Promise to be resolved and then render the view
    await apiRequest.call("/users")
    .then(function (users) {
        console.log(users);
        res.render('usersList.pug', {users: users});
    })
    .catch(function (err) {
        console.log(err);
    })
});

router.get("/new", function(req, res, next) {
    res.render('newUser.pug');
});
// Export this file
module.exports = router;