var mongoose = require("mongoose"),
  Incident = require("../models/Incident");

exports.getAllIncidents = async () => {
  try {
    // Get all the incidents in the database
    const results = await Incident.find({}).sort({'dateTime' : 'desc'});
    return results;
  } catch (error) {
    console.error(`Could not fetch incident ${error}`);
  }
};

exports.addIncident = async (req) => {
  var incident = {
    "title": req.body.title,
    "category": req.body.category,
    "description": req.body.description,
    "user": "no user",
    "status": "Pending"
  };
  try {
    // Create a new incident with the model
    var newIncident = new Incident(incident);
    // Store the incident and wait for the data
    var response = await newIncident.save();
    console.log("Incident added to DB");
    return response;
  } catch (error) {
      console.error("Error adding the new incident to the database: " + error);
  }
};

exports.getIncidentById = async (req) => {
  try {
    // Get the id from the request
    var id = req.params.id;
    // Get the incident from the database
    const incident = await Incident.findById(id);
    return incident;
  } catch (error) {
      console.error("Couldn't find an incident with that id: " + error);
  }
};

exports.getIncidentByTitle = async (req) => {
  try {
    // Get the title from the request
    var title = req.query['title'];
    /* We use the parameter $regex so the mongo database understands the regular expression, 
    using 'i' the search will be insensitive */
    var incidents = Incident.find({ title: { $regex: '.*' + title + '.*', $options: 'i' } });
    return incidents;
  } catch (error) {
      console.error("Error finding an incident with that title: " + error);
  }
};

exports.deleteIncidentById = async (req) => {
  try {
    // Get the id via the request
    var id = req.params.id;
    // Find and delete the incident with the id in the request parameters
    var response = await Incident.findByIdAndDelete(id);
    return response;
  } catch (error) {
      console.error("Error deleting the incident from the database: " + error);
  }
};

exports.updateIncidentById = async (req) => {
  try {
    // Get all the parameters needed
    var id = req.params.id;
    var update = {
      title: req.body.title,
      description: req.body.description,
      category: req.body.category,
      status: req.body.status
    }
    var response = await Incident.updateOne({_id: id}, update);
    return response;
  } catch (error) {
    console.error("Error updating the incident in the database: " + error);
  }
}