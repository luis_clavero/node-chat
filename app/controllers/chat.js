var mongoose = require("mongoose"),
Chat = require("../models/Chat");

exports.getChatMessagesByRoom = async (room) => {
    try {
        var messages = await Chat.find({room: room}).sort({ 'createdAt': 'asc'});
        return messages;
    } catch (error) {
        console.error("Error getting all the messages from the database: " + error);
    }
};

exports.addChat = async (chat) => {
    try {
        var newChat = Chat(chat);
        var response = await newChat.save();
        return response;
    } catch (error) {
        console.error("Can't save new chat message: " + error);
    }
};

exports.addUsername = async (req) => {
    try {
        var username = req.body.username;
        req.session.username = username;
    } catch (error) {
        console.error("No s'ha pogut guardar el nom en sessio");
    }
};