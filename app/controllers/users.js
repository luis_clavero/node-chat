var mongoose = require("mongoose"),
User = require("../models/User");

exports.getAllUsers = async (req) => {
   
   try {
    // results stores the query
    const results = await User.find({});
    //console.log(results)
    //If is API request we want the json
    //but if is web request we don't have to define the res object as 200
    return results
  } catch (error) {
    console.error(`Could not fetch User ${error}`);
  }
};

exports.addUser = async (req) => {
    var user = {
        id: 4,
        username: "user123",
        email: "mail@mail.com",
        password: "djawdakjwda"
    };

    try {
        // Create a new incident with the model
        var newUser = new User(user);
        // Store the incident and wait for the data
        var response = await newUser.save();
        console.log("User added to DB");
        return response;
      } catch (error) {
          console.error("Error adding the new user to the database: " + error);
      }

};

exports.getUserById = async (req) => {

    try{
    var id = req.params.id;
    // to find the id we to the model and use a moongose method that its almost the same as (findOne({ _id: id })
    
    const user = await User.findById(id);
    return user;
    } catch (error) {
        console.error("Couldn't find an incident with that id: " + error);
    }
};



exports.deleteUserById = async (req) => {
    
    // Get the id from the http request
    var id = req.params.id;
    // We delete the user that has the request id by using moongoose method:
    
    const user = await User.findByIdAndDelete(id);
    return user;

    
};