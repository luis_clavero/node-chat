// Requires and paths
var path = require("path");
var controllerDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

// Controllers
var usersController = require(path.join(controllerDir, "users"));
var chatController = require(path.join(controllerDir, "chat"));
var incidentsController = require(path.join(controllerDir, "incidents"));



// Identify requests by API
router.use(function timeLog(req, res, next) {
    req.isApi = true;
    next();
});



// User routes
router.get("/users", async (req, res, next) => {
    var result = await usersController.getAllUsers()
    console.log(result);
    res.jsonp(result);
});
router.post("/users/new", async (req, res, next) => {
    
})
router.get("/users/:id", async (req, res, next) => {
    var result = await usersController.getUserById();
    console.log(result);
    res.jsonp(result);
});
router.delete("/users/:id", async (req, res, next) => {
    var result = await usersController.deleteUserById();
    console.log(result);
    res.jsonp(result);
});



// Chat routes
router.get("/chat/:room", async (req, res, next) => {
    // Get all the messages by it's room
    var response = await chatController.getChatMessagesByRoom(req.params.room);
    console.log(response);
    res.jsonp(response);
});

router.post("/chat", async (req, res, next) => {
    console.log(req.body);
    var response = await chatController.addUsername(req);
    console.log(response);
    
});



// Incident routes
router.get("/incidents", async (req, res, next) => {
    // Wait for the response of the function in the controller
    var response = await incidentsController.getAllIncidents();
    console.log(response);
    // Return the reponse in a json format
    res.jsonp(response)
});

router.post("/incidents/new", async (req, res, next) => {
    console.log(req.body);
    var response = await incidentsController.addIncident(req);
    console.log(response);
    res.jsonp(response);
});

router.get("/incidents/search", async (req, res, next) => {
    var response = await incidentsController.getIncidentByTitle(req);
    console.log(response);
    res.jsonp(response);
});

router.get("/incidents/:id", async (req, res, next) => {
    // Since the function uses the request parameter and it's not a middleware anymore we have to send the req as a parameter of the function
    var response = await incidentsController.getIncidentById(req);
    console.log(response);
    res.jsonp(response);
});

router.put("/incidents/:id", async (req, res, next) => {
    var response = await incidentsController.updateIncidentById(req);
    console.log(response);
    res.jsonp(response);
});

router.delete("/incidents/:id", async (req, res, next) => {
    var response = await incidentsController.deleteIncidentById(req);
    console.log(response);
    res.json(response);
});



// Export this file
module.exports = router;
