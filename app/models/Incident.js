var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IncidentSchema = new Schema({
    title: {type: String, required: true},
    category: {type: String, required: true},
    description: {type: String, required: true},
    dateTime: {type: Date, default: Date.now},
    user: {type: String, required: true},
    status: {type: String, default: "Pending"}
});

module.exports = mongoose.model('Incident', IncidentSchema);