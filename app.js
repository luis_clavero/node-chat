require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");
var router = express.Router();
var mongoose = require("mongoose");
var bodyParser = require('body-parser');
var controllerDir = "/app/app/controllers";
var chatController = require(path.join(controllerDir, "chat"));
var session = require('express-session');

// Start server
var server = require("http").createServer(app).listen(port, () => {
    console.log("Felicidades, el servidor esta funcionando en el puerto: " + port)
});

// Session Setup
app.use(session({
  
    // It holds the secret key for session
    secret: 'Your_Secret_Key',
  
    // Forces the session to be saved
    // back to the session store
    resave: true,
  
    // Forces a session that is "uninitialized"
    // to be saved to the store
    saveUninitialized: true
}))
// Socket config
var io = require('socket.io')(server);
io.on('connection', (socket) => {
    console.log('Un cliente se ha conectado');
    // When the event "newMsg" occurs the message is broadcasted
    socket.on('newMsg', (data) => {
        console.log(data);
        // Broadcast the message to the room of the socket (can only be at 1 room at a time)
        socket.broadcast.to(data.room).emit('newMsg', data);
        // Save the message in the database
        chatController.addChat(data);
    });
    // When the event "newRoom" occurs change the client to the room selected
    socket.on('newRoom', (data) => {
        socket.leaveAll();
        socket.join(data);
        console.log(socket.rooms);
    })
});

// Mongo database
mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database. ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);





// Configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Views folder  
app.set('views', __dirname + '/views/');

// Public folder
app.use(express.static(__dirname + '/public/'));

// Views engine
app.set('view engine', 'pug');

// Routes file
// Base routes
var indexRouter = require("./app/routes/base");
app.use('/', indexRouter);

// Incidents Routes
var incidentsRouter = require("./app/routes/incidents")
app.use('/incidents', incidentsRouter);

// Users Routes
var usersRouter = require("./app/routes/users")
app.use('/users', usersRouter);

// Chat Routes
var chatRouter = require("./app/routes/chat");
app.use('/chat', chatRouter);

// API REST routes
var apiRouter = require("./app/api/index");
app.use('/api', apiRouter);

// Bootstrap and libraries
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));

// Redirects when Not Found
/* This statement must be at the end of the file otherwise
    it won't read any more lines than this one */
app.use(function (req, res, next) {
    res.status(404).render('notfound', {});
});

// Add bootstrap to the view
/* - CSS -
link(rel='stylesheet', href='/css/bootstrap.min.css')

- JS -
link(rel='stylesheet', href='/js/jquery.min.js')
link(rel='stylesheet', href='/js/popper.js')
link(rel='stylesheet', href='/js/bootstrap.min.js')
*/