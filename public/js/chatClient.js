$(document).ready(function () {
  // Initialize socket with IO
  const socket = io();
  // Join the default room "room1"
  socket.emit("newRoom", "room1");

  $("#send").submit(function (e) {
    // Prevent the form to do the default action
    e.preventDefault();
    // Get the text of the input box
    var text = $("#box").val();
    // Get room value
    var roomNumber = $("#room").val();
    // Get the user
    var user = $("#user").val();
    console.log(user);
    // Create the object
    var dataToSend = {room: roomNumber, user: user, message: text };
    // Emit that message
    socket.emit("newMsg", dataToSend);
    // Change the value of the input box to nothing
    $("#box").val("");
  });

  // When someone sends a message show it
  socket.on("newMsg", (data) => {
    console.log(data);
    $("#chat").append(`<p class="message">${data.user}- ${data.message}</p><br>`);
  });

  // Every time the user presses any different room join it
  $("#room").change(function (e) {
    var roomNumber = $("#room").val();
    socket.emit("newRoom", roomNumber);
    // Change the messages every time the room changes
    axios.get('/api/chat/' + roomNumber)
    .then(response => {
      // Erase the content of the chat box
      $("#chat").html("");
      // Iterate the response data and append each message to the box
      for (var messageNumber in response.data) {
        $("#chat").append(`<p class="message">${response.data[messageNumber].user}- ${response.data[messageNumber].message}</p>`);
      }
    });
  });
});
